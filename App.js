/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, TouchableOpacity, Button} from 'react-native';

type Props = {};

export default class App extends Component<Props> {
  constructor (props) {
    super(props);
    this.state = {
      minuteText: '00',
      secondText: '00',
      minute: 0,
      second: 0,
    }
  }

  decreaseTime = () => {
    const second = this.state.second > 0 ? this.state.second - 1 : 59;
    const minute = second === 59 ? this.state.minute - 1 : this.state.minute;
    if (minute >= 0) {
      this.setState({minute, second});
      this.setState({
        minuteText: minute < 10 ? `0${minute}` : `${minute}`,
        secondText: second < 10 ? `0${second}` : `${second}`,
      });
    }
  };

  increaseTime = () => {
    const second = this.state.second < 59 ? this.state.second + 1 : 0;
    const minute = second === 0 ? this.state.minute + 1 : this.state.minute;
    if (minute < 3) {
      this.setState({minute, second});
      this.setState({
        minuteText: minute < 10 ? `0${minute}` : `${minute}`,
        secondText: second < 10 ? `0${second}` : `${second}`,
      });
    } else {
      alert('Rảnh à?');
    }
  };

  reset = () => {
    this.setState({
      minute: 0,
      second: 0,
      minuteText: '00',
      secondText: '00',
    });
  };

  start = () => {
    const refresh = setInterval(() => {
      this.decreaseTime();
      if (this.state.minute === 0 && this.state.second === 0) {
        clearInterval(refresh);
      }
    }, 1000);
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={ styles.main }>
          <Text style={ styles.timer }>{ this.state.minuteText }:{ this.state.secondText }</Text>
          <View style={ styles.upDownButton }>
            <TouchableOpacity
              style={ styles.leftButton }
              onPress={ this.decreaseTime }
            >
              <Text style={ styles.buttonText }>-</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={ styles.rightButton }
              onPress={ this.increaseTime }
            >
              <Text style={ styles.buttonText }>+</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={ styles.action }>
          <TouchableOpacity
            style={ styles.buttonSS }
            onPress={ this.reset }
          >
            <Text style={ styles.textSS }>Reset</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={ styles.buttonSS }
            onPress={ this.start }
          >
            <Text style={ styles.textSS }>Start</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  main: {
    flexDirection: 'column',
    flex: 1,
    paddingTop: 100,
  },
  timer: {
    fontSize: 120,
    color: '#000000',
    flex: 2,
  },
  upDownButton : {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  leftButton: {
    width: '28%',
    height: 60,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    marginTop: 20,
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderLeftWidth: 2,
    borderRightWidth: 1,
    borderColor: '#6d6d6d',
    borderTopLeftRadius: 6,
    borderBottomLeftRadius: 6,
    justifyContent: 'center'
  },
  rightButton: {
    width: '28%',
    height: 60,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    marginTop: 20,
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderLeftWidth: 1,
    borderRightWidth: 2,
    borderColor: '#6d6d6d',
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    justifyContent: 'center'
  },
  buttonText: {
    color: '#6d6d6d',
    fontSize: 50,
  },
  result: {
    fontSize: 20,
    color: '#ff0000',
  },
  action: {
    flexDirection: 'row',
    flex: 1,
    paddingTop: 30,
  },
  buttonSS: {
    backgroundColor: '#48535e',
    width: '20%',
    height: 40,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 20,
    marginRight: 20,
  },
  textSS: {
    color: '#ffffff'
  }
});
