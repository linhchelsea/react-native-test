/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, TouchableOpacity} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
const success = 'This is a prime number';
const fail = 'This is not a prime number';

export default class Prime extends Component<Props> {
  constructor (props) {
    super(props);
    this.state = {
      number: 0,
      result: '',
    }
  }

  checkPrimeNumber = () => {
    let count = 0;
    for (let i = 2; i <= Math.sqrt(this.state.number); i += 1) {
      if (this.state.number % i === 0) count += 1;
    }
    this.setState({ result: this.state.number > 1 && count === 0 ? success : fail });
  };

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={ styles.input }
          placeholder={'Input a number'}
          keyboardType={ 'numeric' }
          onChangeText={ (text) => { this.setState({ number: +text }) }}
        >
        </TextInput>
        <TouchableOpacity
          style={ styles.button }
          onPress={ this.checkPrimeNumber }
        >
          <Text style={ styles.text }>{ 'Check'.toUpperCase() }</Text>
        </TouchableOpacity>
        <Text style={ styles.result }>{ this.state.result }</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  input: {
    width: '60%',
    height: 40,
    borderWidth: 1,
    borderColor: '#6d6d6d',
    borderRadius: 6,
    textAlign: 'center'
  },
  button: {
    width: '70%',
    height: 40,
    backgroundColor: '#6d6d6d',
    alignItems: 'center',
    marginTop: 20,
    borderRadius: 6,
    justifyContent: 'center'
  },
  text: {
    color: '#ffffff',
    fontWeight: 'bold',
  },
  result: {
    fontSize: 20,
    color: '#ff0000',
  }
});
